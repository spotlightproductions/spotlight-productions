We are a full scale video production studio - weve got you covered for concepting, script writing, storyboards, studio hire, directing, filming, editing, sound and consulting. Our technical skills and proven experience across a range of video styles mean we can create the best possible results for your investment.

Website: https://www.spotlightproductions.com.au/
